const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');

// Require passport config file
require('./config/passport')(passport); // passing the passport argument

// Routes
const auth = require('./routes/auth');

const app = express();

app.use('/auth', auth);


app.listen(3000, () => {
    console.log(`Server started at http://localhost:3000`)
});